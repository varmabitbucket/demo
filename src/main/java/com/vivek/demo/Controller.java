package com.vivek.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Controller {

    @RequestMapping("/books")
    public class BookController{
        @Autowired
        private BookService bookService;


        @GetMapping
        public List<Book> findAllBooks(){
//            return bookService.findAllBooks();
            return null;
        }

        @GetMapping("/{bookId}")
        public Book findBook(@PathVariable Long bookId){
//            return bookService.findBookById(bookId);
            return  null;
        }

        @PostMapping
        public Book createBook(@RequestBody Book book){
//            return bookService.createBook(book);
            return null;
        }

        @DeleteMapping
        public void deleteBook(@PathVariable Long bookId){
//            bookService.deleteBook(bookId);
        }

        @PutMapping("/{bookId}")
        public Book updateBook(@RequestBody Book book, @PathVariable Long bookId){

            return null;
        }

    }
}
